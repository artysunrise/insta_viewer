import "babel-polyfill";
import Lib from "./lib";

const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(3000, function () {
  console.log(`Backend App listening on http://localhost:3000`);
});

// ================================

app.get(`/api/accounts/:account`, (req, res) => {
  const account = req.params.account;

  Lib.getAccountData(account).then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(500).send(err);
  });
});
