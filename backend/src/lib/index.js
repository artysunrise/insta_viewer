const request = require('request');

const awaitRequest = (url) => new Promise((res, rej) => {
  request(url, (err, response, body) => {
    if (err) {
      return rej(err);
    }
    res(body);
  });
});

async function getAccountData(login) {
  if (!login) {
    throw new Error("No login provided");
  }

  try {
    const htmlPageData = await awaitRequest(`https://www.instagram.com/${login}`);

    const startScript = '<script type="text/javascript">';
    const endScript = ';</script>';

    const shared_data_start_pos = htmlPageData.indexOf(`${startScript}window._sharedData`) + startScript.length;
    const shared_data_end_pos = htmlPageData.indexOf(endScript, shared_data_start_pos);

    const shared_data = htmlPageData.substring(shared_data_start_pos + "window._sharedData = ".length, shared_data_end_pos);
    const jsoned_shared_data = JSON.parse(shared_data);

    return jsoned_shared_data.entry_data.ProfilePage[0];

  } catch (err) {
    console.log(err);
    return Promise.reject(err);
  }
}

const Lib = {
  getAccountData,
}

export default Lib;
