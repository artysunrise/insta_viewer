var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var PROXY_API_URL = "http://localhost:3000";

module.exports = {
  devtool: 'eval',
  entry: [
    'babel-polyfill',
    'webpack/hot/only-dev-server',
    './src/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  resolve: {
    alias: {
      utils: path.join(__dirname, "src", "utils"),
    },
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      title: "Insta Viewer",
      template: "index.html",
    }),
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['react-hot', 'babel'],
      include: path.join(__dirname, 'src')
    }]
  },

  devServer: {
    publicPath: '/',
    hot: true,
    historyApiFallback: true,
    contentBase: "dist/",
    proxy: {
      "/api*": {
        target: PROXY_API_URL,
      },
    },
    port: 7777,
  }
};
