import {createStore, combineReducers, applyMiddleware} from "redux";
import thunkMiddleware from "redux-thunk";
import AppReducer from "../reducers";

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);
const reducers = {
  AppState: AppReducer,
}
const reducer = combineReducers(reducers);

export function configureStore(initialState) {
  return createStoreWithMiddleware(reducer, initialState,
    window.devToolsExtension && window.devToolsExtension(), // redux-devtools chrome plugin
  );
}
