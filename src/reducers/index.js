import * as Actions from "../actions";

const initialState = {
  requestProccessing: false,
  errorMessage: null,


  accounts: [],
  currentAccount: null,
  currentAccountData: null,

  checkedAccountsState: {},
};


function App(state = initialState, action) {
  switch (action.type) {
    case Actions.ACCOUNTS_LOADED_FROM_FILE:
      return Object.assign({}, state, {
        accounts: action.accounts,
      });
    case Actions.SELECT_ACCOUNT:
      return {
        ...state,
        currentAccount: action.account,
        currentAccountData: null,
      };
    case Actions.REQUEST_START:
      return Object.assign({}, state, {
        requestProccessing: true,
        errorMessage: null,
      });
    case Actions.REQUEST_FINISH:
      return {...state, requestProccessing: false};

    case Actions.ACCOUNT_DATA_LOADED:
      return Object.assign({}, state, {
        requestProccessing: false,
        currentAccountData: action.accountData.user,
      });
    case Actions.ACCOUNT_LOAD_FAILED:
      return {
        ...state,
        errorMessage: action.errorMessage,
      };

    case Actions.TOGGLE_CHECK_ACCOUNT:
      return {
        ...state,
        checkedAccountsState: {
          ...state.checkedAccountsState,
          [action.account]: !state.checkedAccountsState[action.account],
        },
      };

    case Actions.UNCHECK_ALL_ACCOUNTS:
      return {
        ...state,
        checkedAccountsState: {},
      };

    default:
      return state;
  }
}

export default App;
