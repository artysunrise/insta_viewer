import Fetch from "utils/Fetch";
import FileSaver from "file-saver";

export const LOAD_ACCOUNTS_FROM_FILE = "LOAD_ACCOUNTS_FROM_FILE";
export const ACCOUNTS_LOADED_FROM_FILE = "ACCOUNTS_LOADED_FROM_FILE";
export const SELECT_ACCOUNT = "SELECT_ACCOUNT";
export const ACCOUNT_DATA_LOADED = "ACCOUNT_DATA_LOADED";
export const REQUEST_START = "REQUEST_START";
export const REQUEST_FINISH = "REQUEST_FINISH";
export const ACCOUNT_LOAD_FAILED = "ACCOUNT_LOAD_FAILED";
export const TOGGLE_CHECK_ACCOUNT = "TOGGLE_CHECK_ACCOUNT";
export const SAVE_CHECKED_ACCOUNTS = "SAVE_CHECKED_ACCOUNTS";
export const UNCHECK_ALL_ACCOUNTS = "UNCHECK_ALL_ACCOUNTS";

const BASE_URL = "/api/accounts";

function loadDataFromFile(file) {
  return new Promise((res, rej) => {
    const reader = new FileReader();
    reader.onload = (e) => res(e.target.result);
    reader.readAsText(file);
  });
}

export function loadAccountsFromFile(file) {
  return async (dispatch) => {
    try {
      const accountsString = await loadDataFromFile(file);
      const accounts = accountsString.split("\n").map(item => ({account: item}));
      dispatch({type: ACCOUNTS_LOADED_FROM_FILE, accounts});
    } catch (e) {
      console.error(e);
      window.alert(e.message);
    }
  }
}

export function selectAccount(account) {
  return async (dispatch) => {
    dispatch({type: SELECT_ACCOUNT, account});
    dispatch({type: REQUEST_START});
    try {
      const url = `${BASE_URL}/${account}`;

      const accountData = await Fetch(url);
      dispatch({type: ACCOUNT_DATA_LOADED, accountData});
    } catch (e) {
      console.error(e);
      // window.alert(e.message);
      const errorMessage = "Не удалось загрузить аккаунт";
      dispatch({type: ACCOUNT_LOAD_FAILED, errorMessage});
    } finally {
      dispatch({type: REQUEST_FINISH});
    }
  }
}

export function toggleCheckAccount(account) {
  return {
    type: TOGGLE_CHECK_ACCOUNT,
    account,
  }
}

export function saveCheckedAccounts() {
  return (dispatch, getState) => {
    const {AppState: {checkedAccountsState}} = getState();

    let checkedAccountsString = "";

    for (const key in checkedAccountsState) {
      if (checkedAccountsState[key]) {
        checkedAccountsString += `${key}\n`;
      }
    }

    if (!checkedAccountsString) {
      window.alert("Ошибка: ничего не выделено");
      return;
    }

    const file = new File([checkedAccountsString], "accs.txt", {type: "text/plain;charset=utf-8"});
    FileSaver.saveAs(file);
  }
}

export function unCheckAllAccounts() {
  return {
    type: UNCHECK_ALL_ACCOUNTS,
  }
}