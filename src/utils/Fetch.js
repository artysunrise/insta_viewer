export default async function Fetch(url, options = {}) {
  const response = await window.fetch(url, options);

  if (!response.ok) {
    throw new Error(`${response.status}: ${response.statusText}`);
  }

  const resultJSON = await response.json();

  return resultJSON;
}
