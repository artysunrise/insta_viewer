import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import Sidebar from "./views/Sidebar";
import Content from "./views/Content";

import * as ActionCreators from "../actions";

class AppContainer extends React.Component {
  render() {
    const {dispatch, AppState} = this.props;
    const boundActionCreators = bindActionCreators(ActionCreators, dispatch);
    const rootProps = {
      actions: boundActionCreators,
      AppState
    }

    return (
      <div className="container">
        <div className="col-md-3">
          <Sidebar {...rootProps} />
        </div>

        <div className="col-md-8" style={{position: "fixed", right: 0}}>
          <Content {...rootProps} />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(AppContainer);
