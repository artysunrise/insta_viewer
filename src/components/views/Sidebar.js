import React from "react";

class Account extends React.Component {
  toggleCheck(e) {
    this.props.actions.toggleCheckAccount(this.props.item.account);

    // e.preventDefault();
    e.stopPropagation();
  }

  select(e) {
    e.preventDefault();
    e.stopPropagation();

    this.props.actions.selectAccount(this.props.item.account);
  }

  render() {
    const {item: {account}, actions, AppState: {currentAccount, checkedAccountsState}} = this.props;
    const isSelected = currentAccount == account;
    const isChecked = checkedAccountsState[account] ? true : false;

    return (
      <div className="hovered" style={{position: "relative"}} onClick={::this.select}>
        <span style={{fontWeight: isSelected ? "bold" : "initial", cursor: "pointer"}}>
          {account}

        </span>

        <input type="checkbox" checked={isChecked} style={{position: "absolute", right: 20}}
          onClick={::this.toggleCheck} onChange={e => void 0}/>
      </div>
    )
  }
}


class Accounts extends React.Component {
  render() {
    const {AppState: {accounts}} = this.props;
    return (
      <div>
        {
          accounts.map((item, i) => {
            return <Account key={item.account} item={item} {...this.props} />
          })
        }
      </div>
    )
  }
}

export default class Sidebar extends React.Component {
  loadFromFile(e) {
    const file = e.currentTarget.files[0];
    this.props.actions.loadAccountsFromFile(file);
  }

  render() {
    const {AppState: {checkedAccountsState}} = this.props;
    const checkedAtLeastOne = Object.keys(checkedAccountsState).reduce((result, acc) => result || checkedAccountsState[acc], false);

    return (
      <div>
        <button onClick={e => this.refs.file.click()}>Загрузить из файла</button>
        <input type="file" ref="file" onChange={::this.loadFromFile} style={{display:"none"}} />

        {
          checkedAtLeastOne
          ?
            <div>
              <button onClick={e => this.props.actions.saveCheckedAccounts()}>Сохранить в файл</button>
              <button onClick={e => this.props.actions.unCheckAllAccounts()}>Снять выделение</button>
            </div>
          :null
        }

        <Accounts {...this.props} />
      </div>
    )
  }
}
