import React from "react";

class AccountPosts extends React.Component {
  render() {
    const {posts} = this.props;

    return (
      <div style={{display: "flex", justifyContent: "space-around", flexFlow: "row wrap"}}>
        {
          posts.map((item) => {
            const {
              thumbnail_src, code,
              comments: {count: comments_count} = {},
              likes: {count: likes_count} = {},
            } = item;
            return (
              <div key={code} style={{width: 200, height: 200, margin: 10, position: "relative"}}>
                <a href={`http://instagram.com/p/${code}`} target="_blank">
                  <img src={thumbnail_src} alt="Post img" style={{width: "100%"}}/>
                  <div style={{backgroundColor: "#ccc", position: "absolute", bottom: 0, right: 0}}>
                    <div>&#128172; {comments_count}</div>
                    <div>♥ {likes_count}</div>
                  </div>
                </a>
              </div>
            )
          })
        }
      </div>
    )
  }
}

class AccountBlock extends React.Component {
  render() {
    const {
      external_url, biography, username, full_name, profile_pic_url,
      media: {count: media_count, nodes} = {},
      follows: {count: follows_count} = {},
      followed_by: {count: followed_by_count} = {},
    } = this.props.currentAccountData;

    if (!username) {
      return null;
    }

    return (
      <div>
        <div className="row">
          <div className="col-md-3">
            <img src={profile_pic_url} alt="User avatar"/>
          </div>
          <div className="col-md-9">
            <div>
              <h3><a href={`https://instagram.com/${username}`} target="_blank">{username}</a></h3>
            </div>
            <div>
              <strong>{full_name}</strong>
              {biography}
            </div>
            <div>
              <a href={external_url} target="_blank">{external_url}</a>
            </div>
            <div style={{marginTop: 20}}>
              <span style={{marginRight: 10}}>
                <strong>{media_count}</strong> публикаций
              </span>
              <span style={{marginRight: 10}}>
                <strong>{followed_by_count}</strong> подписчиков
              </span>
              <span>
                <strong>{follows_count}</strong> подписок
              </span>
            </div>
          </div>
        </div>

        <div className="row">
          <AccountPosts posts={nodes} />
        </div>
      </div>
    )
  }
}

export default class Content extends React.Component {
  render() {
    const {AppState: {accounts, currentAccountData, currentAccount, requestProccessing, errorMessage}} = this.props;

    if (requestProccessing) {
      return (
        <h1>Загрузка...</h1>
      );
    }

    if (errorMessage) {
      return (
        <h1 className="text-danger">
          {errorMessage} {" "}
          <a href={`https://instagram.com/${currentAccount}`} target="_blank">{currentAccount}</a>
        </h1>
      );
    }

    if (!currentAccountData) {
      return null;
    }

    return (
      <div>
        <AccountBlock currentAccountData={currentAccountData} />
      </div>
    )
  }
}