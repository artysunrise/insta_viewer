import React from "react";
import {configureStore} from "../stores";
import {Provider} from "react-redux";
import AppContainer from "./AppContainer.js";

export default function Root() {
  return (
    <Provider store={configureStore()}>
      <AppContainer />
    </Provider>
  );
}
